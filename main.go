package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	stringResponse := "Method Not OK"
	code := 200
	if request.HTTPMethod == "GET" {
		stringResponse = "Yay a successful response!!"
	}
	APIResponse := events.APIGatewayProxyResponse{Body: stringResponse, StatusCode: code}
	return APIResponse, nil
}
